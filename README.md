## NumberEight Demographic Client

The docker container is available here:

```
registry.gitlab.com/numbereight/community/demographic-client:latest
```

### Running locally
A container can be started directly from the command line using the following command:

```bash
docker run -e "UPSTREAM_TOKEN=REPLACE_WITH_API_KEY" -e "REDIS_CACHE_CONNECTION=redis://my.optional.redis.server:6379" registry.gitlab.com/numbereight/community/demographic-client:latest
```

### Hosting & Costs

Each instance requires at least 500MB RAM and up to 1 vCPU. An instance can service ~500 queries/second (QPS) and the latency has been measured to be < 5ms for the 99th percentile of /v1/demographics requests. In our experience, running 2 of these instances on GCP should roughly cost about $60/month.

Instances can be scaled by CPU usage and we recommend a scaling event at 80% usage. The primary process is not currently multi-threaded. As such providing more than 1vCPU will face diminishing returns - including the possibility of unusable capacity preventing scaling.

### Endpoints
In addition to the endpoints listed under the [HTTPS API](https://demographics.eu.numbereight.ai/docs/) (refer to the documentation you have been sent for the service you are using), the Docker container exposes these additional endpoints to be used as Kubernetes lifecycle hooks:

#### /live

* 200 OK - the service is running
* 500 - the service is not running

#### /ready

* 200 OK - the service is ready to receive requests
* 500 - the service is not ready to receive requests

### Exit codes

* 1  - an unrecoverable error occurred while starting up.  This will also log an appropriate error message.
* 50 - the server has been terminated remotely. This will occur when the cache is in a known bad state.  The server will also log an error message before shutting down.

### Required ports

#### Inbound

* 5000 - an HTTP server is listening on this port. HTTPS should be configured externally to the container. Override using the PORT environment variable.

#### Outbound

* 443 - HTTPS outbound port to NumberEight upstream server

### Variables

| Environment Variable   | Required | Description                                                                                                         |
|------------------------|----------|---------------------------------------------------------------------------------------------------------------------|
| UPSTREAM_TOKEN         | Yes      | Your API key, available from your Customer Portal account                                                           |
| REDIS_CACHE_CONNECTION | No       | (highly recommended) The URL of a [Redis server](https://redis.io/) to enable caching for higher performance, e.g. redis://10.0.0.8:6379 |
| PORT                   | No       | Port to expose the HTTP API on Defaults to 5000                                                                     |
| UPSTREAM_URL           | No       | URL of NumberEight’s upstream server Defaults to http://demographics.eu.numbereight.ai                              |
